var controller = new ScrollMagic.Controller({loglevel: 3})

/* Sections */

var sectionAboutTween = TweenMax.fromTo('#sectionAbout', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});
var sectionAssociatedTween = TweenMax.fromTo('#sectionAssociated', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});
var sectionGlobalTween = TweenMax.fromTo('#sectionGlobal', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});
var sectionBenefitsTween = TweenMax.fromTo('#sectionBenefits', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});
var sectionTechnologyTween = TweenMax.fromTo('#sectionTechnology', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});
var sectionLearnTween = TweenMax.fromTo('#sectionLearn', 0.75, {y: 100, opacity: 0}, {y: 0, opacity: 1, immediateRender: false});

var sectionAboutScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--about', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionAboutTween)
  // .addIndicators()
  .addTo(controller)

var sectionAssociatedScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--associated-companies', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionAssociatedTween)
  // .addIndicators()
  .addTo(controller)

var sectionGlobalScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--global-reach', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionGlobalTween)
  // .addIndicators()
  .addTo(controller)

var sectionBenefitsScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--benefits', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionBenefitsTween)
  // .addIndicators()
  .addTo(controller)

var sectionTechnologyScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--technology-cta', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionTechnologyTween)
  // .addIndicators()
  .addTo(controller)

var sectionLearnScene = new ScrollMagic.Scene({triggerElement: '#sectionTrigger--learn-from-us', duration: 300, loglevel: 3})
  .triggerHook(0.7)
  .setTween(sectionLearnTween)
  // .addIndicators()
  .addTo(controller)

/* Elements */

var companiesStagger = TweenMax.staggerFrom('#sectionAssociated .brand-img', 0.5, {scale: 0.5, opacity: 0}, 0.2);
var aboutSectionPhone1 = TweenMax.fromTo('#phone1Img', 0.75, {x: -100, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});
var aboutSectionPhone2 = TweenMax.fromTo('#phone2Img', 0.75, {x: 100, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});
var aboutSectionPhone2responsive = TweenMax.fromTo('#phone2ImgResponsive', 0.75, {x: 100, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});
var benefitsSectionRow1 = TweenMax.fromTo('.benefits-grid__row.first', 1, {x: 200, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});
var benefitsSectionRow2 = TweenMax.fromTo('.benefits-grid__row.second', 1, {x: -200, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});
var benefitsSectionRow3 = TweenMax.fromTo('.benefits-grid__row.third', 1, {x: 200, opacity: 0}, {x: 0, opacity: 1, immediateRender: false});

var companiesStaggerScene = new ScrollMagic.Scene({triggerElement: '#associatedCompaniesTrigger', duration: 250, loglevel: 3})
  .triggerHook(0.8)
  .setTween(companiesStagger)
  // .addIndicators()
  .addTo(controller)

var aboutSectionPhone1Scene = new ScrollMagic.Scene({triggerElement: '#phone1ImageTrigger', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(aboutSectionPhone1)
  // .addIndicators()
  .addTo(controller)

var aboutSectionPhone2Scene = new ScrollMagic.Scene({triggerElement: '#phone2ImageTrigger', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(aboutSectionPhone2)
  // .addIndicators()
  .addTo(controller)

var aboutSectionPhone2ResponsiveScene = new ScrollMagic.Scene({triggerElement: '#phone2ImageTrigger', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(aboutSectionPhone2responsive)
  // .addIndicators()
  .addTo(controller)

var benefitsSectionRow1Scene = new ScrollMagic.Scene({triggerElement: '.benefits-grid__row.first', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(benefitsSectionRow1)
  // .addIndicators()
  .addTo(controller)

var benefitsSectionRow2Scene = new ScrollMagic.Scene({triggerElement: '.benefits-grid__row.second', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(benefitsSectionRow2)
  // .addIndicators()
  .addTo(controller)

var benefitsSectionRow3Scene = new ScrollMagic.Scene({triggerElement: '.benefits-grid__row.third', ease: Circ.easeIn, duration: 250, loglevel: 3})
  .triggerHook(0.7)
  .setTween(benefitsSectionRow3)
  // .addIndicators()
  .addTo(controller)
