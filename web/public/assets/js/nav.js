var primaryNav = {
  init: function() {
    $('#burgerMenu').on('click', function() {
      $('#mobileNav').toggleClass('show');
    });
  }
};

jQuery(function() {
    primaryNav.init()
});
