var vuejsNews = new Vue({
  el: '#vuejsNews',
  data: function () {
    return {
      'newsItemsArray': null,
      'filterCategories': [],
      'filterSelected': null,
      'currentPage': 0,
      'itemsPerPage': 4
    }
  },
  beforeMount: function () {
    var newsItems = document.querySelectorAll('.dataForVue');
    var newsArray = [];
    newsItems.forEach(item => {
      var newObject = {};
      newObject.category = item.dataset.category;
      newObject.title = item.dataset.title;
      newObject.postdate = item.dataset.postdate;
      newObject.slug = `/Vena_DreamConfig/web/news/${item.dataset.slug}`;
      // http://localhost:8888/Vena_DreamConfig/web/news/internal-13
      if (!this.filterCategories.includes(item.dataset.category)) {
        this.filterCategories.push(item.dataset.category);
      }
      newsArray.push(newObject);
    })

    this.newsItemsArray = newsArray;
  },
  methods: {
    nextPage() {
      // pages are zero index, hence -1
      if (this.currentPage < this.pageCount - 1) {
        this.currentPage++;
      }
    },
    prevPage() {
      if (!this.currentPage <= 0) {
        this.currentPage--;
      }
    },
    setPage(number) {
      if (number) {
        // pages are zero index, hence -1
        this.currentPage = (number - 1);
      }
    },
    setFilter(filter) {
      if (filter) {
        this.filterSelected = filter;
        this.currentPage = 0;
      }
    },
    resetFilter() {
      this.filterSelected = null;
    }
  },
  computed: {
    pageCount() {
      let length = this.filteredData.length;
      let size = this.itemsPerPage;

      return Math.ceil(length / size);
    },
    filteredData() {
      var items = this.newsItemsArray;

      if (this.filterSelected != null && this.filterSelected != undefined) {
        var filtered = items.filter(filter => {
          return filter.category === this.filterSelected;
        });
        return filtered;
      } else {
        return items;
      }
    },
    paginatedData() {
      const start = this.currentPage * this.itemsPerPage;
      const end = start + this.itemsPerPage;
      var items = this.filteredData;
      return items.slice(start, end);
    }
  }
})
